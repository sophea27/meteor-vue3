import { Meteor } from "meteor/meteor";
import Todos from "./collections";

Meteor.publish("todos", () => {
  // console.log('selector', selector)
  const todos = Todos.find();
  // console.log(Genres.fetch())
  return todos;
});
