import { Meteor } from "meteor/meteor";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import { CallPromiseMixin } from "meteor/didericis:callpromise-mixin";
import { EJSON } from "meteor/ejson";
import SimpleSchema from "simpl-schema";

// collections
import Todos from "./collections";

// find
export const getAllTodos = new ValidatedMethod({
  // method name
  name: "app.getAllTodos",
  mixins: [CallPromiseMixin],
  validate: null,
  run() {
    if (Meteor.isServer) {
      try {
        return Todos.aggregate([
          {
            $sort: {
              createdAt: -1,
            },
          },
        ]);
      } catch (error) {
        console.log(error);

        throw new Meteor.Error(
          "getAllTodos Error",
          error.reason,
          EJSON.stringify(error.details)
        );
      }
    }
  },
});
// find one
// create
export const createTodo = new ValidatedMethod({
  // method name
  name: "app.createTodo",
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    doc: Todos.schema,
  }).validator(),
  run({ doc }) {
    if (Meteor.isServer) {
      try {
        return Todos.insert(doc);
      } catch (error) {
        console.log(error);

        throw new Meteor.Error(
          "createTodo Error",
          error.reason,
          EJSON.stringify(error.details)
        );
      }
    }
  },
});
// update
// remove
