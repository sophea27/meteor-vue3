import { reactive } from "vue";
import { createTodo } from "/imports/api/todos/methods";
export default useNew = () => {
  const form = reactive({
    title: "",
    body: "",
  });

  const onCreateTodo = () => {
    const doc = {
      title: form.title,
      body: form.body,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    createTodo
      .callPromise({ doc })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return { form, onCreateTodo };
};
